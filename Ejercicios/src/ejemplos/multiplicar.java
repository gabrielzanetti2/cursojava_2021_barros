package ejemplos;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JList;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.SystemColor;

public class multiplicar extends JInternalFrame {
	private JComboBox cmdTabla;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					multiplicar frame = new multiplicar();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public multiplicar() {
		getContentPane().setBackground(SystemColor.window);
		setBackground(Color.WHITE);
		setTitle("uwu");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		JLabel lblTablasDeMultiplicar = new JLabel("Tablas de multiplicar");
		lblTablasDeMultiplicar.setBounds(161, 11, 102, 14);
		getContentPane().add(lblTablasDeMultiplicar);
		
		cmdTabla = new JComboBox();
		cmdTabla.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}));
		cmdTabla.setBounds(36, 67, 37, 20);
		getContentPane().add(cmdTabla);
		
		JList list = new JList();
		list.setBounds(252, 99, 93, 113);
		getContentPane().add(list);
		
		JButton btnNewButton = new JButton("Calcule");
		btnNewButton.setBounds(43, 143, 89, 23);
		getContentPane().add(btnNewButton);

	}
}
