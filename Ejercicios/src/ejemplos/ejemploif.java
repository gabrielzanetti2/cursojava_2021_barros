package ejemplos;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;

public class ejemploif extends JFrame {

	private JPanel Panel;
	private JLabel text1;
	private JLabel text2;
	private JLabel text3;
	private JTextField textNota1;
	private JTextField textNota2;
	private JTextField textNota3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ejemploif frame = new ejemploif();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ejemploif() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		Panel = new JPanel();
		Panel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(Panel);
		Panel.setLayout(null);
		
		JLabel lblUwu = new JLabel("uwu");
		lblUwu.setHorizontalAlignment(SwingConstants.CENTER);
		lblUwu.setBounds(174, 11, 60, 37);
		Panel.add(lblUwu);
		
		text1 = new JLabel("New label");
		text1.setBounds(39, 80, 46, 14);
		Panel.add(text1);
		
		text2 = new JLabel("New label");
		text2.setBounds(39, 120, 46, 14);
		Panel.add(text2);
		
		text3 = new JLabel("New label");
		text3.setBounds(39, 161, 46, 14);
		Panel.add(text3);
		
		textNota1 = new JTextField();
		textNota1.setBounds(106, 77, 86, 20);
		Panel.add(textNota1);
		textNota1.setColumns(10);
		
		textNota2 = new JTextField();
		textNota2.setBounds(106, 117, 86, 20);
		Panel.add(textNota2);
		textNota2.setColumns(10);
		
		textNota3 = new JTextField();
		textNota3.setBounds(106, 158, 86, 20);
		Panel.add(textNota3);
		textNota3.setColumns(10);
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.setBounds(103, 207, 89, 23);
		Panel.add(btnCalcular);
	}
}
