package ejemplos;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JComboBox;

public class b {

	private JFrame frame;
	private JComboBox Caja;
	private JList Lista;
	private JLabel Titulo;
	private JButton Boton;
	private String[] values = new String[10];
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					b window = new b();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public b() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		Caja = new JComboBox();
		
		String[] arrayStrTablas = new String[10];
		
		for(int i = 0;i<50;i++)
		{
			arrayStrTablas[i] = Integer.toString(i+1);
		}
		
		Caja.setBounds(36, 46, 74, 20);
		frame.getContentPane().add(Caja);
		
		Boton = new JButton("Calcular Tabla");
		Boton.setBounds(162, 227, 109, 23);
		frame.getContentPane().add(Boton);
		
		Titulo = new JLabel("Tablas de Multiplicar");
		Titulo.setBounds(166, 11, 101, 14);
		frame.getContentPane().add(Titulo);
		
		Lista = new JList();
		Lista.setBounds(303, 49, 94, 87);
		frame.getContentPane().add(Lista);
	}
}
