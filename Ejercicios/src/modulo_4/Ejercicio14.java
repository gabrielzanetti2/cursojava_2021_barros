package modulo_4;

import java.util.Scanner;

public class Ejercicio14
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese el puesto en el torneo");
		int Puesto = scan.nextInt();
		
		switch (Puesto)
		{
		case 1:
			System.out.println("Se lleva la medalla de oro, un campeon");
			break;
		case 2:
			System.out.println("Se lleva la medalla de plata, casi campeon");
			break;
		case 3:
			System.out.println("Se lleva la medalla de bronce, suerte la proxima");
			break;
		default:
			System.out.println("No llegaste a ser un capo, segui participandooo");
			break;
		}
		scan=null;
	}
}
