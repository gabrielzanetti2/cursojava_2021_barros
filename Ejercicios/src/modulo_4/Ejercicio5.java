package modulo_4;

import java.util.Scanner;

public class Ejercicio5 
{
	public static void main(String[] args) 
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese el puesto en un valor numerico");
		int Puesto =scan.nextInt();
		
		if(Puesto == 1)
			System.out.println("Se lleva la medalla de oro, un campeon");
		else if(Puesto == 2)
			System.out.println("Se lleva la medalla de plata, casi campeon");
		else if(Puesto == 3)
			System.out.println("Se lleva la medalla de bronce, suerte la proxima");
		else
			System.out.println("No llegaste a ser un capo, segui participandooo");
		
		scan=null;
	}

}
