package modulo_4;

import java.util.Scanner;

public class Ejercicio8 
{
	public static void main(String[] args) 
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("Para la competicion los valores son: \n Piedra = 0 \n Papel = 1 \n Tijera = 2\n");
		System.out.println("Ingrese el valor del primer competidor");
		float PrimerCompetidor=scan.nextFloat();
		System.out.println("Ingrese el valor del segundo competidor");
		float SegundoCompetidor=scan.nextFloat();
		
		if (PrimerCompetidor == SegundoCompetidor)
			System.out.println("Empataron");
		else
			if (PrimerCompetidor == 0)
				if (SegundoCompetidor == 1)
					System.out.println("Gano el segundo competidor");
				else
					System.out.println("Gano el primer competidor");
			else
				if (PrimerCompetidor == 1)
					if (SegundoCompetidor == 0)
						System.out.println("Gano el primer competidor");
					else
						System.out.println("Gano el segundo competidor");
				else
					if (PrimerCompetidor == 2)
						if (SegundoCompetidor == 0)
							System.out.println("Gano el segundo competidor");
						else
							System.out.println("Gano el primer competidor");
		scan=null;
	}
}